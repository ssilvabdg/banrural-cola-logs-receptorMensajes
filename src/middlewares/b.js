var CryptoJS = require('crypto-js');
var config = require('../routes/config');
/**
 * Created by josepablocabreragarcia on 11/05/17.
 * Constantes
 *
 *
 */

module.exports = function () {
    var b = {
        return_: function () {
            return return_();
        },
        let_: function () {
            return let_();
        },
        number_: function () {
            return number_();
        },
        string_: function () {
            return string_();
        },
        a: function () {
            return a();
        },
        KEY_LOGGED_USER: function () {
            return KEY_LOGGED_USER();
        },
        DEFAULT_PUI: function () {
            return DEFAULT_PUI();
        },
        API_ENDPOINT: function () {
            return API_ENDPOINT();
        },
        ulr: function () {
            return ulr();
        },
        C_OK: function () {
            return C_OK();
        },
        C_ERR: function () {
            return C_ERR();
        },
        TIMEOUT: function () {
            return TIMEOUT();
        },
    }

    function return_() {
        return '39DJNCKS4384NR85RSL-U2FsdGVkX1/Mo5SGkxB4Zz9C1409raU2FsdGVkX1+3YARHbvfEz7uR5dany4m9RDPGqYRvbaHtu+ud1MtckEcKzfyxTjhuOLJpoLJQRs';
    }

    function let_() {
        return 'UR4184AR280ZUR390SL-XHKMxVzeiXHtCyMf8fwLBB3aV.e94bj4EOSFwKKIlexRJWqVhMjUYf0il/M4utFUfqLS6ZlEmklOrqsSTT+POL/-JDLI';
    }

    function number_() {
        return '20182039488NC4M0VI9VIMO0S-CIM-fGcGNDfQIg2CUycDbl241U7Mfdls4cMW/Ot6yucv6KyQwa52nkZ1Bu6weU2FsdGVkX18rmLe1XsR+Z639LKj4XGDkilKDU';
    }

    function string_() {
        return 'III120179-6JMDm8xbUIK43Ar92y3VczUnEq1pgKFg=sdAePnYTIun6ASCrjOdiBqUrcmsXUdAhz8=2-marUPQb6CQPDvZLGy+inSoH8tOt1t58tYA1c8T84Hiukhasdof';
    }

    function a() {

        let string_ = CryptoJS.SHA3((this.return_.substring(10, 14) + this.let_.substring(0, 6) + this.number_.substring(11, 18) +
            this.string_.substring(2, 8)), { outputLength: 512 }).toString();

        return string_;
    }

    function KEY_LOGGED_USER() {
        return 'KEY_LOGGED_USER';
    }

    function DEFAULT_PUI() {
        return CryptoJS.AES.decrypt('U2FsdGVkX1/HVVNtk5topH5LFHRLk1Vf/mgPvkvTSyFb5SslxZHv8D9smG352X+I', this.a).toString(CryptoJS.enc.Utf8);
    }

    function API_ENDPOINT() {

        // //WIFFI --- Desarrollo Remoto - - BDG:
        let d = config.urlcapamedia;

        return d;
    }  //ENDPOINTS

    function ulr() {

        let d = CryptoJS.AES.decrypt('U2FsdGVkX186UqJNLMb9C6o1+iwUya1yVsP70G9ym8HnAF/pE5aK5oVS703BlHz9', this.a).toString(CryptoJS.enc.Utf8);
        return d;
    }

    function C_OK() {
        return 0;
    }

    function C_ERR() {
        return 1;
    }

    function TIMEOUT() {
        return 10000;
    }
    
    return b;
}
