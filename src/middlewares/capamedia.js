// import { JSEncrypt } from 'jsencrypt';
// const crypto = require('crypto')
const b = require('./b')();
const http = require('./http')();
// console.log(b.API_ENDPOINT())

/**
 * ***************************************************
 * ************** CONSULTA *********************
 * ***************************************************
 */

module.exports = function () {
    var capamedia = {
        consultarBanrural: function (parametros, endpoint, timeout = 180000, datos = '') {
            return new Promise((resolve, reject) => {
                http.post(parametros, endpoint+"", timeout, datos).then(res => {
                    resolve(res);
                }, err => {
                    reject(err);
                });
            });
        },
        cod: function (parametros) {
            return http.codifi(parametros);
        },
        dec: function (parametros) {
            return http.decodifi(parametros);
        }
    }
    return capamedia
}
