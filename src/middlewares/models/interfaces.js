module.exports = function () {
    var interfacee = {
        EUi: {
            /**
             * Funcion que obtiene el modelo del dispositivo
             */
            pSmartPhone: '',
            /**
             * Funcion que obtiene el modelo del dispositivo
             */
            pTipoSmartPhone: '',
            /**
             * Funcion que obtiene la plataforma del dispositivo
             */
            pSO: '',
            /**
             * Funcion que obtiene el IMEI del dispositivo
             */
            pImei: '',
            /**
             * Funcion que obtiene el UUID del dispositivo
             */
            pUi: '',
            /**
             * Funcion que obtiene la version del APP  **dato estatico**
             */
            pVersionAPP: ''

        },
        ErrorHttp: {
            vCodigo: 0,
            vMensaje: '',
            vMuestraMensaje: false,
            vFinSesion: false,
            vRegresar: false
        },
        globalSPI: {
            globalSPI: {
                Sincronizado: false,
                TipoFactor: 0,
                UUID: "",
                IsRegistro: false,
                Token: "",
                rootPage: "RootPage",
                pSmartPhone: "",
                pSO: "",
                pUsuario: "",
                pVersionAPP: "",
                menBienvenida: "",
                registro: "",
                pToken: "",
                mensaje: ""
            },
            pCode: {
                RegistroMFA: 1,
                AutenticarUsuario: 2,
                RegistrarMFA: 3,
                AutenticaRF: 4

            },
            dispositivo: {
                UUID: "",
                Token: "",
                Factores: [],
                pSmartPhone: "",
                pSO: "",
                Ip: ""
            }
        }
    }
    return interfacee;
}