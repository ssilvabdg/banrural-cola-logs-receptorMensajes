var CryptoJS = require("crypto-js");
const { JSEncrypt } = require('js-encrypt')
const b = require('./b')();
const interfaces = require('./models/interfaces')();
const globalSPI = interfaces.globalSPI.globalSPI;
const axios = require('axios');


module.exports = function () {

    var publicos = 'QW5nTG9yZW1JcHN1bWF5Z2luYWdhbWl0bmFtb2RlbG9uZ2luZHVzdHJpeWFuZ3BhZ3ByaXByaW50YXRwYWd0eXR5cGVz' +
        'ZXQuQW5nTG9yZW1JcDkwc3VtYW5nbmEwMTIxZ2luZ3JlZ3VsYXJuYW1vZGVsb3NpbXVsYXBhbm9vbmcxNTAwcyxub29uZ21heWlzYW5nZGlraWxhbGFuZ21hbmxpbGltYmFnYW5' +
        'ka3VtdWhhbmdnYWxsZXluZ3R5cGVhdGdpbnVsb2FuZ3BhZ2tha2EtYXlvc25pdG91cGFuZ21ha2FnYXdhbmdsaWJyb25nbWdhdHlwZXNwZWNpbWVuLk5hbGFncGFzYW5uaXRvaGl' +
        'uZGlsYW5nbGltYW5nc2lnbG8sa3VuZGluYWxhZ3Bhc2FuZGlubml0b2FuZ3BhZ2xhZ2FuYXA0MjMxbmdlbGVjdHJvbmljdHlwZXNldHRpbmdhdG5hbmF0aWxpbmdwYXJlaGFzLlN' +
        '1bWlrYXRpdG9ub29uZzE5NjBza2FzYWJheW5ncGFnbGFiYXNuZ0xldHJhc2V0c2hlZXRzbmFtYXlyb29uZ21nYXRhbGF0YW5nTG9yZW1JcHN1bSxhdGthbWFrYWlsYW5sYW5nc2F' +
        'tZ2FkZXNrdG9wcHVibGlzaGluZ3NvZnR3YXJldHVsYWRuZ0FsZHVzUGFnZW1ha2VyZ2luYW1pdGFuZ200NDEyZ2FiZXJzeW9ubmdMb3JlbUlwc3VtLkFuZ0xvcmVtSXBzdW1heWd' +
        'pbmFnYW1pdG5hbW9kZWxvbmdpbmR1c3RyaXlhbmdwYWdwcmlwcmludGF0cGFndHl0eXBlc2V0LkFuZ0xvcmVtSXA5MHN1bWFuZ25hZ2luZ3JlZ3VsYXJuYW1vZGVsb3NpbXVsYXB' +
        'hbm9vbmcxNTAwcyxub29uZ21heWlzYW5nZGlraWxhbGFuZ21hbmxpbGltYmFnYW5ka3VtdWhhbmdnYWxsMzQxMmV5bmd0eXBlYXRnaW51bG9hbmdwYWdrYWthLWF5b3NuaXRvdXB' +
        'hbmdtYWthZ2F3YW5nbGlicm9uZ21nYXR5cGVzcGVjaW1lbi5OYWxhZ3Bhc2Fubml0b2hpbmRpbGFuZ2xpbWFuZ3NpZ2xvLGt1bmRpbmFsYWdwYXNhbmRpbm5pdG9hbmdwYWdsYWd' +
        'hbmFwbmdlbGVjdHJvbmljdHlwZXNldHRpbmdhdG5hbmF0aWxpbmdwYXJlaGFzLlN1bWlrYXRpdG9ub29uZzE5NjBza2FzYWJheW5ncGFnbGFiYXNuZ0xldHJhc2V0c2hlZXRzbmF' +
        'tYXlyb29uZ21nYXRhbGF0YW5nTG9yZW1JcHN1bSxhdGthbWFrYWlsYW5sYW5nc2FtZ2FkZXNrdG9wcHVibGlzaGluZ3NvZnR3YXJldHVsYWRuZ0FsZHVzUGFnZW1ha2VyZ2luYW1' +
        'pdGFuZ21nYWJlcnN5b25uZ0xvcmVtSXBzdW0u';

    var codigosResppuesta = {
        'correcto': 0,
        'deslogueado': 10000,
        'erro_logout': 5,
        'lista_vacia': 4,
        'no_productos': 2 //se aplico en prestamos
    };

    // constructor(public http: HttpClient,
    //   private Connectivity: ConnectivityServiceProvider,
    //   private storeProv: LocalstorageProvider,
    //   private device: Device,
    //   public smartId: SmartIdWrapper) {
    // }

    var http_error = interfaces.ErrorHttp;

    
    function post(parametros, endpoint, timeout = 180000, datos = '') {
        return new Promise((resolve, reject) => {
            // if (Connectivity.isOnline()) { ¬fer
            if (true) {
                //endpoint solicitado
                parametros.pCodigo = parseInt(endpoint);
                parametros.pIpUsuario = '181.209.138.4';
                let infoDispositivo = parametros.eUi ? parametros.eUi[0] : null;
                if (!infoDispositivo) {
                    infoDispositivo = interfaces.EUi;
                    infoDispositivo.pSmartPhone = 'KIW-L24';
                    infoDispositivo.pTipoSmartPhone = 'KIW-L24';
                    infoDispositivo.pSO = 'Android';
                    infoDispositivo.pUi = b.DEFAULT_PUI.toString();
                    infoDispositivo.pImei = 'null';
                    //infoDispositivo.pVersionAPP = '3.6';//TODO: Cambiar a la versión que se utilizara
                    infoDispositivo.pVersionAPP = '1.0.1'; //TODO: Cambiar a la versión que se utilizara
                    //infoDispositivo.pVersionAPP = '3.17';//TODO: Cambiar a la versión que se utilizara
                    globalSPI.pVersionAPP = infoDispositivo.pVersionAPP;
                    //pUsuario-> String: usuario almacenado (-1: no logueado)
                    //parametros.pUsuario = '-1';
                    //pSesionId-> String: ID de session almacenada
                    if (parametros.pCodigo == 35) {
                        parametros.pUsuario = '-1';
                        parametros.pSesionId = 'BDR';
                    }
                    //BDR
                    // -1 SESSIONID
                    //pIpUsuario-> String: obtener la IP del dispositivo
                    parametros.pIp = '181.209.138.4';
                    parametros.eUi = [];
                    parametros.eUi.push(infoDispositivo);
                }

                // console.log(parametros)
                let json_envio = JSON.stringify(parametros);
                let vStringB64 = mesEnc(json_envio);
                let op = mesEnc(endpoint);
                let idTel = mesEnc(infoDispositivo.pUi || b.DEFAULT_PUI.toString());
                let peticion_ws = b.API_ENDPOINT() +
                    '?hash1=' + encodeURIComponent(op.toString(CryptoJS.enc.Utf8)) +
                    '&hash2=' + encodeURIComponent(vStringB64.toString(CryptoJS.enc.Utf8)) +
                    '&hash3=' + encodeURIComponent(idTel.toString(CryptoJS.enc.Utf8));

                var headers = {
                    'Content-Type': 'text/plain',
                };

                // headers = headers.set('Content-Type', 'text/plain');
                // headers = headers.set('timeout', '' + timeout);
                // let optionsH = new RequestOptions({
                //   headers: headers
                // });

                if (datos != '') {
                    try {
                        let objSenderMFA = JSON.parse(datos);
                        objSenderMFA.eUi = [];
                        objSenderMFA.eUi.push(infoDispositivo);
                        objSenderMFA.pCodigo = parametros.pCodigo;
                        objSenderMFA.pUsuario = parametros.pUsuario;
                        objSenderMFA.pSesionId = parametros.pSesionId;
                        objSenderMFA.pIpUsuario = parametros.pIpUsuario;
                        objSenderMFA.pIp = parametros.pIp;
                        let jsonDatos = JSON.stringify(objSenderMFA);
                        datos = mesEnc(jsonDatos).toString(CryptoJS.enc.Utf8);
                    } catch (imgEx) {
                        datos = '';
                    }
                }

                axios.post(
                    peticion_ws,
                    datos,
                    {
                        headers,
                        responseType: 'text'
                    },
                ).then(res => {
                    let data = res.data;
                    // console.log('===============================================')
                    // console.log('===============================================')
                    // console.log({
                    //     peticion_ws: peticion_ws,
                    //     datos: datos,
                    //     data: data
                    // })
                    // console.log('===============================================')
                    // console.log('===============================================')
                    //console.log("Respuesta", data);
                    //De base64 a string
                    let respuesta_raw = data;
                    let respuesta_b64 = '';

                    respuesta_b64 = mesDec(data);

                    let e1 = /&Ntilde;/g;
                    let e2 = /&ntilde;/g;
                    let e3 = /&Aacute;/g;
                    let e4 = /&aacute;/g;
                    let e5 = /&Eacute;/g;
                    let e6 = /&eacute;/g;
                    let e7 = /&Iacute;/g;
                    let e8 = /&iacute;/g;
                    let e9 = /&Oacute;/g;
                    let e10 = /&oacute;/g;
                    let e11 = /&Uacute;/g;
                    let e12 = /&uacute;/g;

                    respuesta_b64 = respuesta_b64.replace(e1, 'Ñ')
                        .replace(e2, 'ñ')
                        .replace(e3, 'Á')
                        .replace(e4, 'á')
                        .replace(e5, 'É')
                        .replace(e6, 'é')
                        .replace(e7, 'Í')
                        .replace(e8, 'í')
                        .replace(e9, 'Ó')
                        .replace(e10, 'ó')
                        .replace(e11, 'Ú')
                        .replace(e12, 'ú');

                    try {
                        let json_respuesta = JSON.parse(respuesta_b64.toString());
                        switch (json_respuesta.eMensaje[0].pCodigo) {
                            case codigosResppuesta.erro_logout:
                                // smartId.unLink();
                                http_error = ErrorHttp(json_respuesta.eMensaje[0].pCodigo, json_respuesta.eMensaje[0].pMensaje, json_respuesta.eMensaje[0].pMostrar, false, true);
                                reject(http_error);
                                break;
                            case codigosResppuesta.deslogueado:
                                // smartId.unLink();
                                http_error = ErrorHttp(json_respuesta.eMensaje[0].pCodigo, json_respuesta.eMensaje[0].pMensaje, json_respuesta.eMensaje[0].pMostrar, true, false);
                                reject(http_error);
                                break;
                            case codigosResppuesta.correcto:
                                resolve(json_respuesta);
                                break;
                            case codigosResppuesta.no_productos:
                            case codigosResppuesta.lista_vacia:
                                http_error = ErrorHttp(json_respuesta.eMensaje[0].pCodigo, json_respuesta.eMensaje[0].pMensaje, json_respuesta.eMensaje[0].pMostrar, false, true);
                                reject(http_error);
                                break;
                            default:
                                console.log("Entra en default");
                                http_error = ErrorHttp(json_respuesta.eMensaje[0].pCodigo, json_respuesta.eMensaje[0].pMensaje, json_respuesta.eMensaje[0].pMostrar, false, true);
                                reject(http_error);
                                break;
                        }
                    }
                    catch (err) {
                        console.log(err);
                        reject(ErrorHttp(1, 'Mala lectura de archivo.', true, false, true));
                    }
                }).catch(function (error) {
                    // console.log("Imprimendo antes del error:", error);
                    reject(ErrorHttp(1, 'Ocurrio un problema con la conexión al servidor.', true, false, true));
                });


            } else {
                reject(ErrorHttp(1, 'No posees conexión a Internet.', true, true, true));
            }
        });
    }

    var return_ = 'L84NC4M-SOIEIUSUDJADODROUlVSNE-29dixEOP9-7_P39392KDLKjdsiaspjdjisSEIOJMVNDKlskdkd';
    var let_ = 'w4NE5DNE0w-djksli39djsdlcjsldj9-lsldkdkdk39KDKSDLOEñlsoldoeow03odfvjsOSOS0EOSPodosp';
    var number_ = '-Vsasa83829K/VkkxMjAxN19S-RUR4/RUEO/ROC3mcmckdksiIOEOPSKDLSKSklmckdskdslkdskdeooweo';
    var string_ = 'IDSI99kd0l-0918384N93029jdols.SI.0VI1201OEWOOWEPOEWOPCldslñlldsñldslsdldlsoeoewpolñsl';

    function b64DecodeUnicode(str) {
        // Going backwards: from bytestream, to percent-encoding, to original string.
        return decodeURIComponent(atob(str).split('').map(function (c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    }

    //KGENERATE.thisreturn_(20,30)more let_(0,10)number_more(13,25)

    //(16,19)string_ //(26,30) number_ //(0,7)return_ //(33,40)string_ //(40,44)return_
    function mesDec(cod) {
        try {
            let k = return_.substring(20, 30) + let_.substring(0, 10) + number_.substring(13, 25);
            let kbyte = CryptoJS.enc.Base64.parse(k);
            cod = cod.replace(/(\r\n|\n|\r)/gm, '');
            //datos encriptados como un arreglo de bytes
            let jsDec = CryptoJS.AES.decrypt(cod, kbyte, {
                mode: CryptoJS.mode.ECB,
                padding: CryptoJS.pad.Pkcs7
            });
            //datos encriptados como string
            let jsDecString = jsDec.toString(CryptoJS.enc.Utf8);
            return jsDecString;
        } catch (error) {
            // console.log("Error al decodificar el mensaje:", error);
            return '';
        }
    }

    function mesEnc(jsn) {
        try {
            let kv = string_.substring(16, 19) +
                number_.substring(26, 30) + return_.substring(0, 7) +
                string_.substring(33, 40) + return_.substring(41, 44);
            let jsonEnc = CryptoJS.AES.encrypt(jsn, kv).toString();
            return jsonEnc;
        } catch (error) {
            return '';
        }
    }

    function mesDecECBPKCS7(cod) {
        try {
            let kv = string_.substring(16, 19) +
                number_.substring(26, 30) + return_.substring(0, 7) +
                string_.substring(33, 40) + return_.substring(41, 44) +
                string_.substring(16, 19) + number_.substring(26, 30) + return_.substring(0, 1);
            //let kbyte = CryptoJS.enc.Base64.parse(kv);
            cod = cod.replace(/(\r\n|\n|\r)/gm, '');
            //datos encriptados como un arreglo de bytes
            let jsDec = CryptoJS.AES.decrypt(cod, kv, {
                mode: CryptoJS.mode.ECB,
                padding: CryptoJS.pad.Pkcs7
            });
            //datos encriptados como string
            let jsDecString = jsDec.toString(CryptoJS.enc.Utf8);
            return jsDecString;
        } catch (error) {
            console.log("Error al decodificar el mensaje:", error);
            return '';
        }
    }

    function mesEncECBPKCS7(jsn) {
        try {
            let kv = string_.substring(16, 19) +
                number_.substring(26, 30) + return_.substring(0, 7) +
                string_.substring(33, 40) + return_.substring(41, 44) +
                string_.substring(16, 19) + number_.substring(26, 30) + return_.substring(0, 1);
            let jsonEnc = CryptoJS.AES.encrypt(jsn, kv, { mode: CryptoJS.mode.ECB, padding: CryptoJS.pad.Pkcs7 }).toString();
            return jsonEnc;
        } catch (error) {
            console.log(error);
            return '';
        }
    }

    // PRIVATE
    function getRawIV() {
        const rawIV = forward(4, publicos);
        const ivBuffer = CryptoJS.enc.Utf8.parse(rawIV);
        return ivBuffer;
    }

    // PRIVATE
    function getKey() {
        let kv = string_.substring(16, 19) +
            number_.substring(26, 30) + return_.substring(0, 7) +
            string_.substring(33, 40) + return_.substring(41, 44) +
            string_.substring(16, 19) + number_.substring(26, 30) + return_.substring(0, 1);
        console.log('key', kv);
        const kyBuffer = CryptoJS.enc.Utf8.parse(kv);
        return kyBuffer;
    }

    function forward(long, keyBase64) {
        const words = CryptoJS.enc.Base64.parse(keyBase64);
        const key = CryptoJS.enc.Utf8.stringify(words);
        const longitud = key.length;
        const letra = 97;
        let letraBuscar = String.fromCharCode(letra);
        let contado = 0;
        let corridas = 0;
        let concatenado = '';
        for (let i = 0; i < longitud; i++) {
            if (letraBuscar && key.charAt(i) === letraBuscar) {
                contado++;
                if (contado === 10) {
                    corridas++;
                    concatenado += key.substr(i + 1, long);
                    const numeroLetras = 10;
                    i = i + numeroLetras;
                    contado = 0;
                    if (corridas === 1) {
                        letraBuscar = String.fromCharCode(112);
                    } else if (corridas === 2) {
                        letraBuscar = String.fromCharCode(109);
                    } else if (corridas === 3) {
                        letraBuscar = String.fromCharCode(108);
                    } else {
                        letraBuscar = null;
                    }
                }
            }
        }
        return concatenado;
    }

    function Traducir(jsonText) {
        let encript;

        let encrypt = new JSEncrypt();
        encrypt.setPublicKey(`-----BEGIN PUBLIC KEY-----
      MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6D2yTOBax/huf6D1w9SgjCRgnqR06DAMGD9d0jg+vjtoe1UwxPaxr1hH2ZefI9F92JIMUaTaNRAU/Z7aTqT5PjAHNrNsn7lUeqoK8pnXkaODfK6rzew7AKi5t/HZk5+MJUoLOi6P4rLQEjmbyWLYO9qyjQZvkvamu0EctxpwgL8/ZlpzFVCD1iccH8uEdXRYQDNdC7sbGxSFiq1JC2SNainjRtU/3FKHzdDX6MoGgjB8tP7/DLOxwJVI+8JntxLEPnYtyGK2kA3eRtYvgtAEEKbVj/8rp1dq2NTxPOuBZkpQ+KeJ6iiT1XpBReUGnw5fQOvx4YFjpvWZGa4FAJnGvQIDAQAB
      -----END PUBLIC KEY-----`);
        encript = encrypt.encrypt(jsonText);
        return encript;
    }

    function ErrorHttp(vCodigo, vMensaje, vMuestraMensaje, vFinSesion, vRegresar) {
        return {
            vCodigo: vCodigo,
            vMensaje: vMensaje,
            vMuestraMensaje: vMuestraMensaje,
            vFinSesion: vFinSesion,
            vRegresar: vRegresar
        };
    }

    var http = {
        post: function (parametros, endpoint, timeout = 180000, datos = '') {
            return post(parametros, endpoint, timeout, datos);
        },
        b64DecodeUnicode: function (str) {
            return b64DecodeUnicode(str);
        },
        mesDec: function (cod) {
            return mesDec(cod);
        },
        mesEnc: function (jsn) {
            return mesEnc(jsn);
        },
        mesDecECBPKCS7: function (cod) {
            return mesDecECBPKCS7(cod);
        },
        mesEncECBPKCS7: function (jsn) {
            return mesEncECBPKCS7(jsn);
        },
        forward: function (long, keyBase64) {
            return forward(long, keyBase64);
        },
        Traducir: function (jsonText) {
            return Traducir(jsonText);
        },
        ErrorHttp: function (vCodigo, vMensaje, vMuestraMensaje, vFinSesion, vRegresar) {
            return ErrorHttp(vCodigo, vMensaje, vMuestraMensaje, vFinSesion, vRegresar);
        },
        codifi: function (parametros) {
            // let infoDispositivo = parametros.eUi[0];
            // let json_envio = JSON.stringify(parametros);
            // let vStringB64 = mesEnc(json_envio);
            // let op = mesEnc(endpoint);
            // let idTel = mesEnc(infoDispositivo.pUi || b.DEFAULT_PUI.toString());
            // let peticion_ws = b.API_ENDPOINT() +
            //     '?hash1=' + encodeURIComponent(op.toString(CryptoJS.enc.Utf8)) +
            //     '&hash2=' + encodeURIComponent(vStringB64.toString(CryptoJS.enc.Utf8)) +
            //     '&hash3=' + encodeURIComponent(idTel.toString(CryptoJS.enc.Utf8));
            let json_envio = JSON.stringify(parametros);
            let vStringB64 = mesEncECBPKCS7(json_envio);
            return vStringB64.toString(CryptoJS.enc.Utf8);
        },
        decodifi: function (parametros) {
            let data = parametros;
            //console.log("Respuesta", data);
            //De base64 a string
            let respuesta_raw = data;
            let respuesta_b64 = '';
            respuesta_b64 = mesDecECBPKCS7(data);
            let e1 = /&Ntilde;/g;
            let e2 = /&ntilde;/g;
            let e3 = /&Aacute;/g;
            let e4 = /&aacute;/g;
            let e5 = /&Eacute;/g;
            let e6 = /&eacute;/g;
            let e7 = /&Iacute;/g;
            let e8 = /&iacute;/g;
            let e9 = /&Oacute;/g;
            let e10 = /&oacute;/g;
            let e11 = /&Uacute;/g;
            let e12 = /&uacute;/g;

            respuesta_b64 = respuesta_b64.replace(e1, 'Ñ')
                .replace(e2, 'ñ')
                .replace(e3, 'Á')
                .replace(e4, 'á')
                .replace(e5, 'É')
                .replace(e6, 'é')
                .replace(e7, 'Í')
                .replace(e8, 'í')
                .replace(e9, 'Ó')
                .replace(e10, 'ó')
                .replace(e11, 'Ú')
                .replace(e12, 'ú');

            try {
                let json_respuesta = JSON.parse(respuesta_b64.toString());
                return json_respuesta;

                // switch (json_respuesta.eMensaje[0].pCodigo) {
                //     case codigosResppuesta.erro_logout:
                //         // smartId.unLink();
                //         http_error = ErrorHttp(json_respuesta.eMensaje[0].pCodigo, json_respuesta.eMensaje[0].pMensaje, json_respuesta.eMensaje[0].pMostrar, false, true);
                //         return {error: true, body:http_error}
                //     case codigosResppuesta.deslogueado:
                //         // smartId.unLink();
                //         http_error = ErrorHttp(json_respuesta.eMensaje[0].pCodigo, json_respuesta.eMensaje[0].pMensaje, json_respuesta.eMensaje[0].pMostrar, true, false);
                //         return {error: true, body:http_error}
                //     case codigosResppuesta.correcto:
                //         // console.log(json_respuesta.vCodigo)
                //         return {error: false, body:json_respuesta}
                //     case codigosResppuesta.no_productos:
                //     case codigosResppuesta.lista_vacia:
                //         http_error = ErrorHttp(json_respuesta.eMensaje[0].pCodigo, json_respuesta.eMensaje[0].pMensaje, json_respuesta.eMensaje[0].pMostrar, false, true);
                //         return {error: true, body:http_error}
                //     default:
                //         console.log("Entra en default");
                //         http_error = ErrorHttp(json_respuesta.eMensaje[0].pCodigo, json_respuesta.eMensaje[0].pMensaje, json_respuesta.eMensaje[0].pMostrar, false, true);
                //         return {error: true, body:http_error}
                // }
            }
            catch (err) {
                console.log(err);
                return { error: true, body: ErrorHttp(1, 'Mala lectura de archivo.', true, false, true) }
            }
        }
    }
    return http;
}
