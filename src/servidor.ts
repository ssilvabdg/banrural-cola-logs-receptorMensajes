import express, { Express } from 'express';
import cors from 'cors';
import { createServer, Server as ServerHttp } from 'http';
import https from 'https';
export class Servidor {
    private app: Express;
    private port: string;
    private httpServer: ServerHttp;

    constructor() {
        this.app = express();
        this.port = `${process.env.PORT}`;
        this.httpServer = createServer( this.app );
        this.configureServer().then().catch(err => {
            console.log('No se ha iniciado debido al error: ', err);
        })
    }

    async configureServer() {
        await this.middlewares();
        await this.routes();
        await this.start();
    }

    async middlewares() {
        this.app.use(express.json({limit: '50mb'}));
        this.app.use(express.urlencoded({limit:'50mb'}))
        this.app.use(cors());
    }

    async routes() {
        this.app.use('/add-mensaje', require('./routes/mensaje.route'));
        this.app.use('/consultar-mensaje', require('./routes/mensaje2.route'));
        this.app.use('/bitacoraLog/', require('./routes/mensaje4.route'));
        this.app.use('/eliminar-mensaje', require('./routes/mensaje3.route'));
    }

    async start() {
        this.httpServer.listen(this.port, () => {
            return console.log(`server is listening on ${this.port}`);
        });
    }

}