import { Router } from 'express';
import Ctrl from '../controllers/mensaje.controller';

const router = Router();

router.post('/', Ctrl.mensaje);


module.exports = router;