import { Request, Response } from "express";
import { Socket } from "socket.io";
import { Encript } from '../encript'
import logCtrl from './log.controller';

const mensaje = async (req: Request, reso: Response) => {

    const axios = require('axios');
    const desencriptar = new Encript();
    const varEncriptado = `${process.env.Encriptado}`;
    var reqEntrante = req.body;

    console.log("**************************************MENSAJE ENTRANTE************************************* ");
    console.log(req.body);
    console.log("******************************************************************************************* ");
    if (varEncriptado === '1') {
        reqEntrante = desencriptar.decodifi(req.body.d);
    }

    try {
        var capamedia = require('../middlewares/capamedia')();
        capamedia.consultarBanrural(req.body,853).then((respu:any) =>{
            reso.send(respu);
            console.log("**************************************RESPUESTA SERVICIO************************************* ");
            console.log(respu);
            console.log("******************************************************************************************* ");
        }).catch(function(error:any){
            reso.send(error);
            console.log("OCURRIO UN ERROR AL ELIMINAR LOS MENSAJES EN LA COLA", error);
        });
    } catch (err) {
        reso.status(400).json({
            codigo: 4,
            mensaje: `Error grave al eliminar mensaje en cola`
        });
        console.log("ERROR GRAVE AL ELIMINAR MENSAJE EN COLA ", err);
    }
}


const Ctrl = {
    mensaje
}

export default Ctrl;