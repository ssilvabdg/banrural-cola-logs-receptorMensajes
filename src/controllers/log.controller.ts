const escribirBitacoraLog=(usuario:string,componente:component,descripcion:string,status:status)=>{
    const axios = require('axios');
    const fechaActual= new Date();
    var data={
    login:usuario,
    FechaHora:fechaActual,
    Componente:componente,
    Descripcion:descripcion,
    Status:status
    }
    console.log("datos enviados",data);
    axios.post('http://140.254.1.16/BancaMovil/WebService/api_chatbot_mensajes/api/chatBot/bitacoraLog/', data)
    .then((res:any) => {
    console.log("SE GUARDO EL REGISTRO EN LA BITACORA",res.data);
}).catch((err:any) => {
    console.log("OCURRIO UN PROBLEMA INSERTANDO EL REGISTRO EN LA BITACORA", err);
});

}

enum component{
    socket = "socket IO",
    iaISOFT = "IA ISOFT",
    apiReceptor="Api receptor",
    apiCola="API cola de mensajes"
  }

enum status{
    exitoso="200",
    fallido="400"
}  

const log={escribirBitacoraLog,component,status}
export default log;