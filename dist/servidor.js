"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Servidor = void 0;
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const http_1 = require("http");
class Servidor {
    constructor() {
        this.app = (0, express_1.default)();
        this.port = `${process.env.PORT}`;
        this.httpServer = (0, http_1.createServer)(this.app);
        this.configureServer().then().catch(err => {
            console.log('No se ha iniciado debido al error: ', err);
        });
    }
    configureServer() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.middlewares();
            yield this.routes();
            yield this.start();
        });
    }
    middlewares() {
        return __awaiter(this, void 0, void 0, function* () {
            this.app.use(express_1.default.json({ limit: '50mb' }));
            this.app.use(express_1.default.urlencoded({ limit: '50mb' }));
            this.app.use((0, cors_1.default)());
        });
    }
    routes() {
        return __awaiter(this, void 0, void 0, function* () {
            this.app.use('/add-mensaje', require('./routes/mensaje.route'));
            this.app.use('/consultar-mensaje', require('./routes/mensaje2.route'));
            this.app.use('/bitacoraLog/', require('./routes/mensaje4.route'));
            this.app.use('/eliminar-mensaje', require('./routes/mensaje3.route'));
        });
    }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            this.httpServer.listen(this.port, () => {
                return console.log(`server is listening on ${this.port}`);
            });
        });
    }
}
exports.Servidor = Servidor;
