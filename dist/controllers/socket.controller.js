"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SocketCtrl = void 0;
const encript_1 = require("../encript");
const connection = (socket) => {
    const idSocket = socket.id;
    //console.log(idSocket);
    const armado = `{\"message\":{\"sender\": {\"socket\":\"${idSocket}\"}}}`;
    const armado2 = `{\"sender\": {\"socket\":\"${idSocket}\"}}`;
    //console.log(armado);
    const idSocket2 = JSON.parse(armado);
    const idSocket22 = JSON.parse(armado2);
    var idSocket2Aux;
    //console.log("JSON" ,idSocket2)
    const armadoE = `{\"message\":{\"sender\": {\"socket\":\"${idSocket}\",\"error\":\"S\"}}}`;
    const armadoE2 = `{\"sender\": {\"socket\":\"${idSocket}\",\"error\":\"S\"}}`;
    const idError = JSON.parse(armadoE);
    const idError2 = JSON.parse(armadoE2);
    var idErrorAux;
    var log4js = require('log4js');
    const axios = require('axios');
    const varEncriptado = `${process.env.Encriptado}`;
    const desencriptar = new encript_1.Encript();
    socket.on('auth', (id) => {
        console.log("ESTE ES EL ID ", id);
        var separacion = id.split("#");
        console.log("LOGIN ", separacion[1]);
        console.log("ID DISPOSITIVO ", separacion[0]);
        const data = {
            login: separacion[1],
            idDispositivo: separacion[0]
        };
        if (separacion[2] == "S") {
            axios.post('http://140.254.1.16/BancaMovil/WebService/api_chatbot_mensajes/api/chatBot/EliminarMensajes/', data)
                .then((reso) => {
                console.log("SE BORRAN LOS MENSAJES AL INICIO");
                console.log('EMITIENDO EVENTO', `mensaje-${separacion[0]}`);
                socket.join("dispositivosConectados");
                idSocket2Aux = idSocket2;
                if (varEncriptado === '1') {
                    idSocket2Aux = desencriptar.codifi(idSocket2);
                    socket.emit(`mensaje-${separacion[0]}`, { d: idSocket2Aux });
                }
                else {
                    socket.emit(`mensaje-${separacion[0]}`, { message: idSocket22 });
                }
            }).catch((err) => {
                console.log("SE PRESENTO EL SIGUIENTE ERROR AL MOMENTO DE ELIMINAR LOS MENSAJES AL INICIO", err);
            });
        }
        else {
            axios.post('http://140.254.1.16/BancaMovil/WebService/api_chatbot_mensajes/api/chatBot/ConsultarMensajes/', data)
                .then((res) => {
                console.log(`Status: ${res.status}`);
                //BORRAR MENSAJES
                var mensajes = "";
                var i = 0;
                var aux = "";
                var aux2 = "";
                axios.post('http://140.254.1.16/BancaMovil/WebService/api_chatbot_mensajes/api/chatBot/EliminarMensajes/', data)
                    .then((reso) => {
                    console.log("SE BORRAN LOS MENSAJES ENVIADOS");
                    if (res.data.resultado.code == 1) {
                        console.log("TRAIGO DATA");
                        console.log("DATA", res.data);
                        console.log('EMITIENDO EVENTO', `mensaje-${separacion[0]}`);
                        socket.join("dispositivosConectados");
                        idSocket2Aux = idSocket2;
                        if (varEncriptado === '1') {
                            idSocket2Aux = desencriptar.codifi(idSocket2);
                            socket.emit(`mensaje-${separacion[0]}`, { d: idSocket2Aux });
                        }
                        else {
                            socket.emit(`mensaje-${separacion[0]}`, { message: idSocket22 });
                        }
                        mensajes = res.data.detalle;
                        console.log("SE BORRAN LOS MENSAJES ENVIADOS");
                        while (i < mensajes.length) {
                            try {
                                //ENVIAR MENSAJES REZAGADOS
                                //console.log("ESTA ES LA POSIBLE DATA NULL ", mensajes[i].Mensaje);
                                //aux = Buffer.from(mensajes[i].Mensaje, 'base64').toString();
                                //console.log("CADENA EN STRING ", aux);
                                //console.log("CADENA EN PARSE ", JSON.parse(aux));
                                //aux2 = JSON.parse(aux);
                                if (varEncriptado === '1') {
                                    aux2 = desencriptar.decodifi(mensajes[i].Mensaje);
                                    aux2.sender.socket = socket.id;
                                    aux2 = JSON.stringify(aux2);
                                    aux2 = `{\"message\":${aux2}}`;
                                    aux2 = JSON.parse(aux2);
                                    aux2 = desencriptar.codifi(aux2);
                                    socket.emit(`mensaje-${separacion[0]}`, { d: aux2 });
                                }
                                else {
                                    aux = Buffer.from(mensajes[i].Mensaje, 'base64').toString();
                                    aux2 = JSON.parse(aux);
                                    aux2.sender.socket = socket.id;
                                    socket.emit(`mensaje-${separacion[0]}`, { message: aux2 });
                                }
                                console.log("**************************************************************************************************");
                                console.log("MENSAJE ENVIADO CORRECTAMENTE ", aux2);
                                console.log("**************************************************************************************************");
                                i = i + 1;
                            }
                            catch (error) {
                                console.log("FALLO AL REENVIAR EL MENSAJE CON ID ", mensajes[i].Id);
                                idErrorAux = idError;
                                if (varEncriptado === '1') {
                                    idErrorAux = desencriptar.codifi(idError);
                                    socket.emit(`mensaje-${separacion[0]}`, { d: idErrorAux });
                                }
                                else {
                                    socket.emit(`mensaje-${separacion[0]}`, { message: idError2 });
                                }
                                //SE ENVIA MENSAJE DE ERROR VUELVE A INTENTAR
                                return;
                            }
                        }
                    }
                    else {
                        //console.log("NO TRAIGO DATA");
                        //console.log('EMITIENDO EVENTO', `mensaje-${separacion[0]}`);
                        //socket.join("dispositivosConectados");
                        //socket.emit(`mensaje-${separacion[0]}`, { message: idSocket2 })
                    }
                }).catch((err) => {
                    console.log("SE PRESENTO EL SIGUIENTE ERROR AL MOMENTO DE ELIMINAR LOS MENSAJES REZAGADOS", err);
                });
            }).catch((err) => {
                console.log("OCURRIO UN ERROR CONSULTANDO LOS MENSAJES EN COLA");
                console.error(err);
            });
        }
        /*try{
            console.log('EMITIENDO EVENTO', `mensaje-${id}`);
            socket.join("dispositivosConectados");
            socket.emit(`mensaje-${id}`, { message: idSocket2 })
        }catch(error){
            var log = log4js.getLogger("CONEXION_SOCKETID");
            log.fatal(`DESCRIPCION: RECIBIENDO PETICION DE CONEXION - USUARIO: ${idSocket}`);
            console.log(`RECIBIENDO PETICION DE CONEXION DE ${idSocket}`);

        }*/
    });
    socket.on('desconectar', () => {
        try {
            socket.leave("dispositivosConectados");
            socket.disconnect();
            console.log(`EL USUARIO ${idSocket} SE HA DESCONECTADO`);
        }
        catch (error) {
            var log = log4js.getLogger("DESCONEXION_SOCKETID");
            log.fatal(`DESCRIPCION: RECIBIENDO PETICION DE DESCONEXION - USUARIO: ${idSocket}`);
            console.log(`RECIBIENDO PETICION DE DESCONEXION DE ${idSocket}`);
        }
    });
    socket.on('conectado', (idSocket) => {
        const sockets = socket.in("dispositivosConectados").allSockets();
        var conectado = false;
        for (let i in sockets) {
            if (i == idSocket) {
                conectado = true;
            }
        }
        if (conectado) {
            console.log("CONECTADO");
        }
        else {
            console.log("DESCONECTADO");
        }
    });
    //socket.on('parapruebas', parapruebas);
};
/*const parapruebas = () => {
    // socket.emit('disconnect', { message: '¡Hasta luego Amigo! 😀' } );
    console.log('Mensaje de prueba');
}*/
exports.SocketCtrl = {
    connection
    //parapruebas
};
