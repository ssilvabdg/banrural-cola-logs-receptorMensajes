"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const encript_1 = require("../encript");
const mensaje = (req, reso) => __awaiter(void 0, void 0, void 0, function* () {
    const axios = require('axios');
    const desencriptar = new encript_1.Encript();
    const varEncriptado = `${process.env.Encriptado}`;
    var reqEntrante = req.body;
    console.log("**************************************MENSAJE ENTRANTE************************************* ");
    console.log(req.body);
    console.log("******************************************************************************************* ");
    if (varEncriptado === '1') {
        reqEntrante = desencriptar.decodifi(req.body.d);
    }
    try {
        var capamedia = require('../middlewares/capamedia')();
        capamedia.consultarBanrural(req.body, 851).then((respu) => {
            reso.send(respu);
            console.log("**************************************RESPUESTA SERVICIO************************************* ");
            console.log(respu);
            console.log("******************************************************************************************* ");
        }).catch(function (error) {
            reso.send(error);
            /*reso.status(400).json({
                codigo: 4,
                mensaje: `No se pudieron guardar los mensajes en la cola`
            });*/
            console.log("OCURRIO UN ERROR AL GUARDAR EL MENSAJE EN LA COLA", error);
        });
    }
    catch (err) {
        reso.status(400).json({
            codigo: 4,
            mensaje: `Error grave al guardar mensaje en cola`
        });
        console.log("ERROR GRAVE AL GUARDAR MENSAJE EN COLA ", err);
    }
});
const Ctrl = {
    mensaje
};
exports.default = Ctrl;
