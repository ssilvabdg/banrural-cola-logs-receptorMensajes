"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const escribirBitacoraLog = (usuario, componente, descripcion, status) => {
    const axios = require('axios');
    const fechaActual = new Date();
    var data = {
        login: usuario,
        FechaHora: fechaActual,
        Componente: componente,
        Descripcion: descripcion,
        Status: status
    };
    console.log("datos enviados", data);
    axios.post('http://140.254.1.16/BancaMovil/WebService/api_chatbot_mensajes/api/chatBot/bitacoraLog/', data)
        .then((res) => {
        console.log("SE GUARDO EL REGISTRO EN LA BITACORA", res.data);
    }).catch((err) => {
        console.log("OCURRIO UN PROBLEMA INSERTANDO EL REGISTRO EN LA BITACORA", err);
    });
};
var component;
(function (component) {
    component["socket"] = "socket IO";
    component["iaISOFT"] = "IA ISOFT";
    component["apiReceptor"] = "Api receptor";
    component["apiCola"] = "API cola de mensajes";
})(component || (component = {}));
var status;
(function (status) {
    status["exitoso"] = "200";
    status["fallido"] = "400";
})(status || (status = {}));
const log = { escribirBitacoraLog, component, status };
exports.default = log;
