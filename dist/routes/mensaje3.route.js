"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const mensaje3_controller_1 = __importDefault(require("../controllers/mensaje3.controller"));
const router = (0, express_1.Router)();
router.post('/', mensaje3_controller_1.default.mensaje);
module.exports = router;
