"use strict";
var difhoras = 0;
module.exports = {
    /**
     * ===============================================================================
     * ========================= H O R A - A C T U A L ===============================
     * ===============================================================================
     */
    ENCRYPT: true,
    horas: difhoras,
    dateNow: () => {
        const tiempoTranscurrido = Date.now();
        var hoy = new Date(tiempoTranscurrido);
        hoy.setHours(hoy.getHours() + difhoras);
        return hoy;
    },
    /**
     * ===============================================================================
     * =============================== U R L ´ S =====================================
     * ===============================================================================
     */
    // DESARROLLO IDCANAL 1962
    urlcapamedia: 'http://10.160.209.244/interfazCapaMediaR/messageFeed',
    urlreceptormsj: `http://10.160.209.235/`,
    urlbdgapp: `http://35.243.171.66:5023/webhook`, // url socialmanager/bdg-chat-app -> webhook (livechat)
    // QA IDCANAL 1767
    //urlcapamedia: 'http://10.161.208.80/interfazCapaMediaR/messageFeed', // url capa media
    //urlreceptormsj: `http://10.160.118.32/`, //`http://10.160.208.235/`, // url del receptor de mensajes
    //urlbdgapp: `http://172.17.0.1:5022/webhook` // url socialmanager/bdg-chat-app -> webhook (livechat)
    // PRODUCCION IDCANAL 1767
    //urlcapamedia: 'https://bancamovil.banrural.com.gt/interfazCapaMediaR/messageFeed', // url capa media
    //urlreceptormsj: `https://chatbot.banrural.com.gt`, // url del receptor de mensajes
    //urlbdgapp: `https://brbdg-chat-app.isoft-ste.com/webhook` // webhook del bot
};
