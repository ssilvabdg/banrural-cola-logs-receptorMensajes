
# BANRURAL-RECEPTOR-MENSAJES

Banrural-receptor-mensajes es un servidor rest con socket.io para proveer datos a app-mundial.

## Empezando

* Debes tener la versión [16.14.1](https://nodejs.org/) de Node LTS.
* Clona este repositorio: `git clone https://gitlab.com/bdg.sa/banrural-receptor-mensaje.git`.
* Crea un archivo con extensión .env en la raíz del proyecto y configuralo basado en .example.env que se encuentra en la raíz.
* Ejecuta `npm install` desde la raíz del proyecto.
* Ejecuta `npm start` en una terminal desde la raíz del proyecto.
* Profit. :tada:

## License
[MIT](https://choosealicense.com/licenses/mit/)
